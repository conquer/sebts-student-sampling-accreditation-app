<?php
require('access.php');
// Define possible actions for this API
$possible_url = array("get_initial", "get_courses", "get_assignments", "copy_files", "read_summaries", "build_tii_url");

define("S", chr(32)); // Define whitespace constant

$response = array("error" => "an error occured");

if (isset($_GET['action']))
	$action = $_GET['action'];
elseif ($request_body = json_decode(file_get_contents('php://input'), true)) {
	$action = $request_body['action'];
}

if(isset($action) && in_array($action, $possible_url)) {
	switch ($action) {
		case "get_initial":
			$response = array("yearterms" 	=> get_yearterm(),
							  "degrees"  	=> get_degrees(),
							  "curriculums" => get_curriculums()
							  );
		break;
		case "get_courses":
			if(isset($_GET['yearterm'])) {
				$response = get_courses($_GET['yearterm']);  
			}
		break;
		case "get_assignments":
			if(isset($_GET['yearterm']) && isset($_GET['courses'])) {
				if(!$response = get_assignments($_GET['yearterm'], $_GET['courses'])) 
					$response = array("noassignments" => "No assignments found for ".$_GET['courses']);
			}
		break;
		case "copy_files":
			if(isset($request_body['assignments']) 
		   	&& isset($request_body['yearterm'])
			&& isset($request_body['courses'])) {
				if(!isset($request_body['curriculum'])) $request_body['curriculum'] = '';
				$response = copy_files($request_body['assignments'], $request_body['yearterm'], $request_body['courses'], $request_body['degree'], $request_body['curriculum'], $request_body['randomstudent']);
			}
		break;
		case "read_summaries":
			$response = read_summaries();
		break;
		case "build_tii_url":
			$url = build_tii_url(array(	"CFG_TIIAccountID" => $_GET['CFG_TIIAccountID'],
										"tiiObjectID" => $_GET['tiiObjectID'],
										"tiiAssignmentID" => $_GET['tiiAssignmentID'],
										"tiiAssignmentTitle" => $_GET['tiiAssignmentTitle'],
										"tiiCourseTitle" => $_GET['tiiCourseTitle'],
										"tiiCourseID" => $_GET['tiiCourseID'],
										"ownerTIIUserID" => $_GET['ownerTIIUserID'],
										"ownerEmail" => $_GET['ownerEmail'],
										"ownerFName" => $_GET['ownerFName'],
										"ownerLName" => $_GET['ownerLName'],
										"CFG_TIISecretKey" => $_GET['CFG_TIISecretKey'],
										"CFG_TIIAPIURL" => $_GET['CFG_TIIAPIURL'],
										"mdlusridStudent" => $_GET['mdlusridStudent'],
										"tiiSubmissionFileName" => $_GET['tiiSubmissionFileName']));
			header("Location: $url");
		break;
	}
} else $response['error'] = "Invalid action";

function read_summaries(){
	$files = preg_grep('/\.txt/', scandir("./summaries"));
	$data = array();
	foreach($files as $file){
		$contents = file_get_contents("./summaries/".$file);
		$tmp = json_decode($contents, true);
		$data[] = array("id" => str_replace(".txt", "", $file),
						"data" => $tmp['meta_data']);
	}
	rsort($data);
	return $data;
}


function get_yearterm() {
	$sql = 'SELECT * FROM  dbo.listAcademicYearTerms(0,5) order by year desc';
	$return = array();
	foreach(query_campus6($sql) as $row)
		$return[] = array(	"value" => substr($row['Term'], 0, 2).$row['Year'],
							"text" => $row['Year'].S.$row['Term']);
	return $return;
}

function get_degrees() {
	$sql = "SELECT * FROM code_degree WHERE STATUS = 'A'";
	$return = array();
	foreach(query_campus6($sql) as $row)
		$return[] = array(	"text" => $row['LONG_DESC'],
							"value" => $row['CODE_VALUE_KEY']);
	return $return;
}

function get_curriculums() {
	$sql = "SELECT *
		    FROM   code_curriculum
		    WHERE  STATUS = 'A'";
	$return = array();
	foreach(query_campus6($sql) as $row)
		$return[] = array(	"text" => $row['LONG_DESC'],
							"value" => $row['CODE_VALUE_KEY']);
	return $return;
}

function get_courses($yearterm) {
	$sql = "SELECT DISTINCT SUBSTRING_INDEX(shortname, '.', 1) AS courseName
	      	FROM mdl_course crse
		    WHERE shortname LIKE '%-$yearterm'
		    ORDER BY shortname";
	$return = array();
	foreach(query_moodle($sql) as $row)
		$return[] = array(	"text" => $row['courseName'],
							"value" => $row['courseName']);
	return $return;
}

function get_assignments($yearterm, $courses) {
	$sql = "CALL PARCEGetAssignmentsForCourse('$courses.%-$yearterm')";
	$return = array();
	foreach(query_moodle($sql) as $row) {
		$assignmentName = preg_replace("/[^A-Za-z0-9#_ ]/", '', $row['AssignmentName']);
		$return[] = array(	"text" => $assignmentName.S."(".$row['CourseSection'].")",
							"shorttext" => $assignmentName,
							"value" => $row['AssignmentID'],
							"type" => $row['AssignmentType']);
	}
	if(sizeof($return)){
		return $return;
	} else {
		return false;
	}
}

function copy_files($assignments, $yearterm, $course, $degree, $curriculum, $random_sample = false ) {

	$year = substr($yearterm, 0, 4);
	$term = substr($yearterm, 5);

	if($random_sample == "yes") { // do this part if "Random Student Sample" is checked on the UI

		$random_student_list = array();
		$QID;

		if($curriculum == '')
			$random_student_list = query_campus6("SEBTS_APPS.dbo.PARCE_RandomStudentSampling $year, $term, $course, $degree, ''");
		else
			$random_student_list = query_campus6("SEBTS_APPS.dbo.PARCE_RandomStudentSampling $year, $term, $course, $degree, $curriculum");

		foreach($random_student_list as $student) $peopleids[] = $student['People_ID'];
		$comma_delimited_list = implode(",", $peopleids);

		if($comma_delimited_list != "") {
			$sql = "CALL PARCEPrepStudentIDs('$comma_delimited_list')";
			$QID = query_moodle($sql);
			$QID = $QID[0]['qid'];
		} else return array("error" => "No Students in this Population.");
	} 

	//	Loop through assignments IDs and get files based on type
		$parse_data = array();
		foreach($assignments as $assignment) {
			$assignmentID = $assignment['value'];
			$assignmentType = $assignment['type'];
			$assignmentName = $assignment['text'];
			switch ($assignment['type']) {
				case 1:
				case 37: // Normal Assignment
					$parse_data["assignment"][$assignmentName] = query_moodle("CALL PARCEGetAssignSubmissionData($assignmentID, $assignmentType, '$QID')");
					break;
				case 13: // Quiz
					$parse_data["quiz"][$assignmentName] = query_moodle("CALL PARCEGetQuizSubmissionData($assignmentID, '$QID')");
					break;
				case 27: // Turnitin
					$parse_data["turnitin"][$assignmentName] = query_moodle("CALL PARCEGetTIISubmissionData($assignmentID, '$QID')");
					break;	
			}
		}


	// 	Selection data
		$date = date_create();
		$parse_data['meta_data'] = array(	"assignments" => $assignments,
												"yearterm" => $yearterm,
												"course" => $course,
												"degree" => $degree,
												"random_sample" => $random_sample,
												"timestamp" => date_format($date, 'm/d/y g:i A'));

	//	"Assignment" type: copy files to folder
		if(isset($parse_data['assignment'])){	

			$directory = '\\\\file03\\moodle$\\sacs\\'.$year."_".$term."_".$course;
			$dirstatus = shell_exec("IF exist ".$directory." ( echo exists ) ELSE ( mkdir ".$directory." && echo created)");

			if(trim($dirstatus) == "exists" || trim($dirstatus) == "created") {

				foreach ($parse_data['assignment'] as $name => &$assignment) {

					foreach($assignment as &$file){
						$origin = "\\\\file03\\moodle$\\Moodledata\\filedir\\".$file['mainFolder']."\\".$file['subFolder']."\\".$file['sourceFileName'];
						$destination = $directory."\\".$file['destFileName'];
						$file['copystatus'] = shell_exec("copy ".$origin.S.'"'.$destination.'"');	
						$file['fullpath'] = $destination;
					}
				}
			 }
	}

//	Write data to file
	$id = uniqid();
	file_put_contents("summaries/".$id.".txt", json_encode($parse_data));

//	Return ID of file for data retrieval 
	return array("id" => $id);
}

function build_tii_url($info) {
	defined("TII_ENCRYPT") or define("TII_ENCRYPT","0");
	defined("TII_APISRC") or define("TII_APISRC","12");
	defined("TII_DIAGNOSTIC") or define("TII_DIAGNOSTIC","0");
	defined("TII_FCMD") or define("TII_FCMD","2");
	defined("TII_FID") or define("TII_FID","7");
	defined("TII_APILANG") or define("TII_APILANG","en_us");
	defined("TII_ADMINUSERTYPE") or define("TII_ADMINUSERTYPE","2");

	$tiiGMTime=gmdate('YmdH',time());
	$tiiGMTime.=substr(gmdate('i',time()),0,1);

	$assigndata=array('gmtime'=>$tiiGMTime
					 ,'encrypt'=>TII_ENCRYPT
					 ,'aid'=>$info["CFG_TIIAccountID"]
					 ,'diagnostic'=>TII_DIAGNOSTIC
					 ,'fcmd'=>TII_FCMD
					 ,'oid'=>$info["tiiObjectID"]
					 ,'utp'=>TII_ADMINUSERTYPE
					 ,'fid'=>TII_FID
					 ,'assignid'=>$info["tiiAssignmentID"]
					 ,'assign'=>$info["tiiAssignmentTitle"]
					 ,'ctl'=>$info["tiiCourseTitle"]
					 ,'cid'=>$info["tiiCourseID"]
					 ,'uid'=>$info["ownerTIIUserID"]
					 ,'tem'=>$info["ownerEmail"]
					 ,'uem'=>$info["ownerEmail"]
					 ,'ufn'=>$info["ownerFName"]
					 ,'uln'=>$info["ownerLName"]
					 );

	$assigndata['md5']=doMD5($assigndata,$info["CFG_TIISecretKey"]);
	$assigndata['src']=TII_APISRC;
	$assigndata['apilang']=TII_APILANG;

	$keys = array_keys($assigndata);
	$values = array_values($assigndata);
	$querystring='';
	for ($i=0;$i<count($values); $i++) {
		if ($i!=0) {
			$querystring .= '&';
		}
		$querystring .= $keys[$i].'='.urlencode($values[$i]);
	}
	$apiurl = $info["CFG_TIIAPIURL"]."?".$querystring;

	$studentid = $info["mdlusridStudent"];
	$filename = $info["tiiSubmissionFileName"];

	return $apiurl;
}


function doMD5($post,$salt) {
	$output="";
	ksort($post);
	$postKeys=array_keys($post);
	for ($i=0;$i<count($post);$i++) {
		$thisKey=$postKeys[$i];
		$output.=$post[$thisKey];
	}
	$output.=$salt;	
	return md5($output);
}

function query_moodle($sql) {
// 	Connect to server and db
	$moodle_server = 'xxxxxx';
	$moodle  = new mysqli($moodle_server, 'xxx', 'xxxx', "xxx");

	if ($moodle->connect_errno) {
	    die("Failed to connect to MySQL: (" . $moodle->connect_errno . ") " . $moodle->connect_error);
	}

//	Query
	$result = $moodle->query($sql) or die("{'error':'$moodle->error'");

//	Fill an array with the results
	$return_array = array();
	$result->data_seek(0);
	while ($row = $result->fetch_assoc()) $return_array[] = $row;

	$moodle->close();
	$result->close();

//	Return array of results
	return $return_array;
}

function query_campus6($sql) {
// 	Connect to server
	$campus6_server = 'xxxx,xxxx';
	$campus6  = mssql_pconnect($campus6_server, 'xxx', 'xxx') or die("Unable to connect to database: ");

//	Connect to database		
	mssql_select_db("Campus6", $campus6);

//	Query
	$result = mssql_query($sql) or die(mssql_get_last_message());

//	Fill an array with the results
	$return_array = array();
	while ($row = mssql_fetch_assoc($result)) $return_array[] = $row;

//	Return array of result		
	return $return_array;
}

// End program and output json encoded data in the response
exit(json_encode($response));

?>