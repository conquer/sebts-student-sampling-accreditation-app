var parseapp = angular.module('Parse', [])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
  	  when('/', {templateUrl: './home.php', controller: ParseController}).
      when('/summary/:sid', {templateUrl: './summary.php',   controller: SummaryController}).
      when('/summaries/', {templateUrl: './sumlist.php',   controller: SummaryListController}).
      otherwise({redirectTo: '/'});
}]);

function SummaryListController($location, $scope, $http, $rootScope) {
	$http({method: 'GET', url: './api.php', params: {"action": "read_summaries"}})
		.success(function (data) {
			$scope.summaries = data;
			$scope.error = data.error;
		}).error(function(data, status){
			$scope.error = status;
			});

	$scope.startOver = function() {
		$rootScope.init = false;
		$location.path("/");
	}
}

function SummaryController($location, $scope, $http, $routeParams, $rootScope) {
	$scope.loading = true;
$http({method: 'GET', url: './summaries/'+ $routeParams.sid + ".txt"})
		.success(function (data) {
			$scope.summarydata = data;
			$scope.selected_info = data.meta_data;
			delete $scope.summarydata.meta_data;
			$scope.loading = false;
			$scope.error = data.error;
		}).error(function(data, status){
			$scope.error = status;
			});

	// $scope.buildTiiUrl = function() {
	// 	$http({method: 'GET', url: './api.php', data: {"action": "build_tii_url", }})
	// 	.success(function (data) {
	// 		$scope.error = data.error;
	// 	}).error(function(data, status){
	// 		$scope.error = status;
	// 		});
	// }

	$scope.startOver = function() {
		$rootScope.init = false;
		$location.path("/");
	}

	$scope.activeTab = function (item) {
		return item === $scope.selectedTab ? 'active' : undefined;
	}

	$scope.activeTab = function (item, first) {
		if(first) $scope.selectedTab = item;
		return item === $scope.selectedTab ? 'active' : undefined;
	}
}

function ParseController($location, $scope, $http, $rootScope) {
	// Load inital data
	if(!$rootScope.init) {
		$http({method: 'GET', url: './api.php', params: {"action": "get_initial"}})
		.success(function (data) {
			$rootScope.init = true;
			$scope.yearterms = data.yearterms;
			$scope.degrees = data.degrees;
			$scope.curriculums = data.curriculums;
			$scope.error = data.error;
		})
		.error(function(data, status){
			$scope.error = data.error;
		});

		$scope.selectedYearTerm = "";
		$scope.randomstudentsample = "no";
	}

	$scope.loading = false;

	$scope.allStudents = function() {
		$scope.degrees.sel = "";
		$scope.curriculums.sel = "";
	}

	$scope.getCourses = function(){
		$http({method: 'GET', url: './api.php', params: {"action":"get_courses", "yearterm": $scope.selectedYearTerm.value}})
		.success(function (data) {
			$scope.courses = data;
		}).error(function(data, status){
			$scope.error = status;
			});
	}

	$scope.getAssignments = function(){
		$http({method: 'GET', url: './api.php', params: {"action":"get_assignments", 
			"yearterm": $scope.selectedYearTerm.value, 
			"courses": $scope.courses.sel}})
		.success(function (data) {
			$scope.assignments = data;
			$scope.noassignments = data.noassignments;
		}).error(function(data, status){
			$scope.error = status;
			});
	}

	$scope.copyFiles = function() {
		$scope.loading = true;
		$http({method: 'POST', url: './api.php', 
		 data: {"action":"copy_files",
		 	"assignments" : $scope.selectedAssignments,
			"yearterm": $scope.selectedYearTerm.text, 
			"courses": $scope.courses.sel, 
			"curriculum" : $scope.curriculums.sel,
			"degree" : $scope.degrees.sel,
			"randomstudent": $scope.randomstudentsample }})
		.success(function (data) {						
				if(data.error == undefined) {
				    $location.path( "summary/" + data.id );
				} 
				$scope.loading = false;
				$scope.error = data.error;
		}).error(function(data, status){
			$scope.error = "Error: " + status;
			});
	}

	$scope.selYearTerm = function (yearterm) {
		$scope.selectedYearTerm = yearterm;
		$scope.getCourses();
	}

	$scope.yeartermActive = function (item) {
		return item === $scope.selectedYearTerm ? 'active' : undefined;
	}

}

parseapp.filter('capitalize', function() {
    return function(input, scope) {
        return input.substring(0,1).toUpperCase()+input.substring(1);
    }
});