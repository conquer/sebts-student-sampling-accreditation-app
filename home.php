<?php require('access.php'); ?>
	<div ng-show="error" class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<p><b>{{error}}</b></p>
	</div>
	<div class="row-fluid">
		<div class="span2">		
		<div class="well" style="max-width: 340px; padding: 3px 0;">
          <ul class="nav nav-list">
			<li><a ng-href="#/summaries"><i class="icon-list">&nbsp;</i> Summary List</a></li>
			<li class="divider"></li>
			  <li class="nav-header">Year/Term</li>
			  <li ng-repeat="yearterm in yearterms" ng-class="yeartermActive(yearterm)">
			  	<a ng-click="selYearTerm(yearterm)" href="" class="hoverhand">{{yearterm.text}}</a></li>
			  <li class="nav-header">Options</li>
			  <li>				  	
			  <label class="radio">
				<input type="radio" ng-model="randomstudentsample" name="randomstudents" value="yes"> Random Sample
				</label>
				<label class="radio">
				<input type="radio" ng-click="allStudents()" ng-model="randomstudentsample"name="randomstudents" value="no"> All Students
				</label>
				</li>
			<li class="divider"></li>
			<li>
			<button ng-hide="loading" class="btn btn-block" ng-click="copyFiles()" ng-class="loadStatus()">Get Assignments</button>
			<button ng-show="loading" class="btn btn-block disabled">Loading...</button>
			</li>
			<li>&nbsp;</li>
          </ul>
        </div>				
		</div>
<div class="span10">
<div class="row-fluid">
	<div class="span5">
		<fieldset>
			<legend>Courses</legend>
		<span ng-show="selectedYearTerm">
		<input type="text" ng-model="courses.search" placeholder="Filter...">
		<select size="6" ng-model="courses.sel" ng-change="getAssignments()"
			ng-options="course.value as course.text for course in courses | filter:courses.search">
		</select>
		</span>
		<span ng-hide="selectedYearTerm"><p class="muted">Select Year/Term...</p></span>
		</fieldset>
	</div>
	<div class="span5">
		<fieldset>
		<legend>Assignments</legend>
		<span ng-show="courses.sel && !noassignments">
		<input type="text" ng-model="assignsearch.$" class="pull-left" placeholder="Filter...">
		<div class="btn-group pull-right">
		  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		    Type
		    <span class="caret"></span>
		  </a>
		  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
			  <li><a href="#">
				<label class="radio"> All
				<input type="radio" name="assigntype" selected ng-model="assignsearch.type">
				</label>
			</a>
				</li>	
			  <li class="divider"></li>
			  <li><a href="#"><label class="radio"> Quiz
					<input type="radio" name="assigntype" value="13" ng-model="assignsearch.type">
					</label></a></li>
			  <li><a href="#"><label class="radio"> Turnitin
				<input type="radio" name="assigntype" value="27" ng-model="assignsearch.type">
				</label></a></li>						
			  <li>	<a href="#"><label class="radio"> Assignments
					<input type="radio" name="assigntype" value="37" ng-model="assignsearch.type">
					</label></a></li>
			</ul>
		</div>
		<select size="6" ng-model="$root.selectedAssignments" multiple="true" style="width:100%" 
			ng-options="asg as asg.text for asg in assignments | filter:assignsearch">
		</select>
		</span>
		<span ng-hide="courses.sel"><p class="muted">Select Courses...</p></span>
		<span ng-show="noassignments && courses.sel"><p class="text-warning">{{noassignments}}</p></span>
		</fieldset>	
	</div>
</div>
<div class="row-fluid" ng-show="randomstudentsample == 'yes'">
	<div class="span5">
		<fieldset>
		<legend>Degree</legend>
		<input type="text" ng-model="degrees.search" placeholder="Filter...">
		<select size="6" ng-model="degrees.sel"
			ng-options="degree.value as degree.text for degree in degrees | filter:degrees.search">
		</select>
		</fieldset>
	</div>
	<div class="span5">
		<fieldset>
		<legend>Curriculum</legend>
		<input type="text" ng-model="curriculums.search" placeholder="Filter...">
		<select size="6" ng-model="curriculums.sel"
			ng-options="cur.value as cur.text for cur in curriculums | filter:curriculums.search">
		</select>
		</fieldset>
	</div>
</div>
</div>