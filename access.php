<?php
//put sha1() encrypted password here - example is 'hello'
$password = 'a1a5cb0f94b9057d7d7032736422f6564cad4850';

session_start();
if (!isset($_SESSION['loggedIn'])) {
    $_SESSION['loggedIn'] = false;
}

if (isset($_POST['password'])) {
    if (sha1($_POST['password']) == $password) {
        $_SESSION['loggedIn'] = true;
    } else {
        die ('Incorrect password');
    }
} 

if (!$_SESSION['loggedIn']): ?>
<!DOCTYPE html>
<html>
<head>
	<title>Parse App</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
</head>
<body> 
<div class="container-fluid" style="padding-top:10%;">
	<div class="row-fluid">
		<div class="span4">&nbsp;</div>
		<div class="span4 well" align="center">
   <form method="post" class="form-inline">
    <h2><small>Enter Password</small></h2>
	<input type="password" name="password" placeholder="Password">
    <input type="submit" class="btn">
    </form>
</div>
		<div class="span4">&nbsp;</div>
</div>
</div>
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
</body>
</html>
 
<?php
exit();
endif;
?>