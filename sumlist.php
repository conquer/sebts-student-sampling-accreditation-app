<?php require('access.php'); ?>
	<div class="row-fluid">
	<div class="span2">		
		<div class="well" style="max-width: 340px; padding: 3px 0;">
          <ul class="nav nav-list">
			<li><a ng-click="startOver()" ng-href="#"><i class="icon-refresh">&nbsp;</i> Start Over</a></li>
        	<li class="divider"></li> 
			<li>
			<input type="text" ng-model="$root.summarysearch" placeholder="Filter..." style="width:90%">
			</li>
        </ul>
        </div>				
		</div>
		<div class="span10">
<table class="table table-striped table-hover">
	<thead>
	<tr>
		<th colspan="3">Summaries</th>
	</tr>
	</thead>
	<tbody>
		<tr>
			<th>Link</th>
			<th>Timestamp</th>
			<th>Year/Term</th>
			<th>Course</th>
			<th>Assignments</th>
		</tr>
	<tr ng-repeat="summary in summaries | filter:summarysearch">
		<td><a href="#/summary/{{summary.id}}">{{summary.id}}</a></td>
		<td>{{summary.data.timestamp}}</td>
		<td>{{summary.data.yearterm}}</td>
		<td>{{summary.data.course}}</td>
		<td style="overflow:hidden"><ul><li ng-repeat="assignment in summary.data.assignments">{{assignment.shorttext}}</li></ul></td>
	</a>
	</tbody>
</table>
</div>
</div>