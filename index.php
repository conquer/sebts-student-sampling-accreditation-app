<?php require('access.php'); ?>
<!DOCTYPE html>
<html ng-app="Parse">
<head>
	<title>Parse App</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
	<style>
		select {
			height: 200px !important;
			width: 80%;
			background:transparent;
			/*outline:none !important;*/
		}
		.hoverhand:hover {
			cursor:pointer;
		}
	</style>
</head>
<body> 
<div class="container-fluid" style="padding-top:10px;" ng-view>


</div>
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.1.3/angular.min.js"></script>
	<script src="app.js"></script>
</body>
</html>