<?php require('access.php'); ?>
	<div ng-show="error" class="alert alert-error">
		<p><b>Error recieving data from server: {{error}}</b></p>
	</div>
	<div class="row-fluid">
		<div class="span2">		
		<div class="well" style="max-width: 340px; padding: 3px 0;">
          <ul class="nav nav-list">
			<li><a ng-href="#/summaries"><i class="icon-list">&nbsp;</i> Summary List</a></li>
			<li><a ng-click="startOver()" ng-href="#"><i class="icon-refresh">&nbsp;</i> Start Over</a></li>
          	<li class="divider"></li> 
			<h5>Selected Info</h5>
			<li class="nav-header">Year/Term</li>
			<li>{{selected_info.yearterm}}</li>
			<li class="nav-header">Course</li>
			<li>{{selected_info.course}}</li>
			<li ng-show="selected_info.degree" class="nav-header">Degree</li>
			<li>{{selected_info.degree}}</li>
			<li ng-show="selected_info.curriculum" class="nav-header">Curriculum</li>
			<li>{{selected_info.curriculum}}</li>
			<!--<li class="nav-header">Assignments</li>
			 <li ng-repeat="assignment in summarydata.selected_info.assignments">
				<small class="muted">{{assignment.shorttext}}</small></li>-->
          	<li class="divider"></li> 
			<li>
			<input type="text" ng-model="$root.summarysearch" placeholder="Filter..." style="width:90%">
			</li>
        </ul>
        </div>				
		</div>
<div class="span10">
	<p ng-hide="summarydata">Loading...</p>
	<div class="tabbable"> 
  <ul class="nav nav-tabs">
    <li ng-repeat="(key, val) in summarydata" ng-class="activeTab(val, $first)" ng-click="selectedTab = val">
    	<a href="#{{key}}" data-toggle="tab">{{key|capitalize}}</a>
    </li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane" ng-class="activeTab(summarydata.assignment)" id="assignment">
<table class="table table-striped" ng-repeat="(assignmentname,files) in summarydata.assignment">
	<thead>
	<tr>
		<th colspan="3">{{assignmentname}}</th>
	</tr>
	</thead>
	<tbody>
		<tr>
			<th>Student</th>
			<th>File Name</th>
			<th>Status</th>
		</tr>
	<tr ng-repeat="file in files | filter:summarysearch" ng-class="{error:file.DataAvailable == 0}">
		<td>{{file.powercampusID}}</td>
		<td ng-show="file.DataAvailable == 1">{{file.destFileName}}</td>
		<td ng-show="file.DataAvailable == 1">{{file.copystatus}}</td>
		<td ng-show="file.DataAvailable == 0">No Data Available for this Student</td>
		<td ng-show="file.DataAvailable == 0">&nbsp;</td>
	</tr>
	</tbody>
</table>
    </div>
    <div class="tab-pane" ng-class="activeTab(summarydata.quiz)" id="quiz">
<table class="table table-striped" ng-repeat="(quizname,files) in summarydata.quiz">
	<thead>
	<tr>
		<th colspan="3">{{quizname}}</th>
	</tr>
	</thead>
	<tbody>
		<tr>
			<th>Student</th>
			<th>URL</th>
		</tr>
	<tr ng-repeat="file in files | filter:summarysearch" ng-class="{error:file.DataAvailable == 0}">
		<td>{{file.powercampusID}}</td>
		<td ng-show="file.DataAvailable == 1"><a target="quizwindow" ng-href="{{file.url}}">{{file.url}}</a></td>
		<td ng-show="file.DataAvailable == 0">No Data Available for this Student</td>
	</tr>
	</tbody>
</table>
    </div>
    <div class="tab-pane" ng-class="activeTab(summarydata.turnitin)" id="turnitin">
<table class="table table-striped" ng-repeat="(turnitinname,files) in summarydata.turnitin">
	<thead>
	<tr>
		<th colspan="3">{{turnitinname}}</th>
	</tr>
	</thead>
	<tbody>
		<tr>
			<th>Student</th>
			<th>Filename</th>
		</tr>
	<tr ng-repeat="file in files | filter:summarysearch" ng-class="{error:file.DataAvailable == 0}">
		<td>{{file.powercampusID}}</td>
		<td ng-show="file.DataAvailable == 1"><a target="download" ng-href="api.php?action=build_tii_url&CFG_TIIAccountID={{file.CFG_TIIAccountID}}&tiiObjectID={{file.tiiObjectID}}&tiiAssignmentID={{file.tiiAssignmentID}}&tiiAssignmentTitle={{urlencode(file.tiiAssignmentTitle)}}&tiiCourseTitle={{file.tiiCourseTitle}}&tiiCourseID={{file.tiiCourseID}}&ownerTIIUserID={{file.ownerTIIUserID}}&ownerEmail={{file.ownerEmail}}&ownerFName={{file.ownerFName}}&ownerLName={{file.ownerLName}}&CFG_TIISecretKey={{file.CFG_TIISecretKey}}&CFG_TIIAPIURL={{file.CFG_TIIAPIURL}}&mdlusridStudent={{file.mdlusridStudent}}&tiiSubmissionFileName={{file.tiiSubmissionFileName}}">{{file.tiiSubmissionFileName}}</a></td>
		<td ng-show="file.DataAvailable == 0">No Data Available for this Student</td>
	</tr>
	</tbody>
</table>
    </div>
  </div>
</div>
</div>